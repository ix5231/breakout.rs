extern crate piston_window;
extern crate cgmath;
extern crate collision;
extern crate opengl_graphics;
extern crate graphics;

use piston_window::*;
use cgmath::{Point1, Point2, Vector2, Basis2, Rotation2, InnerSpace, Rotation};
use collision::{Aabb2, Aabb};
use opengl_graphics::{GlGraphics, OpenGL};

type Scalar = f64;
type Hitbox = Aabb2<Scalar>;
type Color = [f32; 4];

const BAR_Y_POS: Scalar = 0.9;
const WINDOW_WIDTH: u32 = 600;
const WINDOW_HEIGHT: u32 = 600;

trait DrawSquareHitbox {
    fn hitbox(&self) -> Hitbox;
    fn color(&self) -> Color;
    fn draw(&self, args: &RenderArgs, gl: &mut GlGraphics) {
        let hitbox = self.hitbox();

        gl.draw(args.viewport(), |c, g| {
            // TODO use vector
            graphics::rectangle(self.color(),
                      [hitbox.min.x * (WINDOW_WIDTH as Scalar),
                      hitbox.min.y * (WINDOW_HEIGHT as Scalar),
                      (hitbox.max.x - hitbox.min.x) * (WINDOW_WIDTH as Scalar),
                      (hitbox.max.y - hitbox.min.y) * (WINDOW_HEIGHT as Scalar)],
                      c.transform, g);
        })
    }
}

trait Collidable {
    fn collide(&self, target: &Self) -> bool;
}

impl Collidable for Hitbox {
    fn collide(&self, target: &Hitbox) -> bool {
        target.to_corners().iter().map(|&p| self.contains(p)).fold(false, |acc, c| acc || c) ||
        self.to_corners().iter().map(|&p| target.contains(p)).fold(false, |acc, c| acc || c)
    }
}

fn gen_aabb_with_radius(center: Point2<Scalar>, radius: Vector2<Scalar>) -> Hitbox {
    Aabb2::new(center + (-radius), center + radius)
}

struct Bar {
    pos: Point1<Scalar>,
    hitbox: Hitbox,
}

impl Bar {
    pub fn update(&mut self, new_pos: Point1<Scalar>) {
        self.pos = new_pos;
        self.hitbox = self.gen_hitbox();
    }

    pub fn gen_hitbox(&self) -> Hitbox {
        gen_aabb_with_radius(self.center(), Bar::radius())
    }

    pub fn center(&self) -> Point2<Scalar> {
        Point2::new(self.pos.x, BAR_Y_POS)
    }

    #[inline(always)]
    fn radius() -> Vector2<Scalar> {
        Vector2::new(0.1, 0.0125)
    }
}

impl DrawSquareHitbox for Bar {
    fn hitbox(&self) -> Hitbox {
        self.hitbox
    }

    fn color(&self) -> Color {
        [1.0, 0.0, 0.0, 1.0]
    }
}

impl Default for Bar {
    fn default() -> Bar {
        let bar_default_pos = 0.5;
        let bar_default_center = Point2::new(bar_default_pos, BAR_Y_POS);

        Bar {
            pos: Point1::new(bar_default_pos),
            hitbox: gen_aabb_with_radius(bar_default_center, Bar::radius())
        }
    }
}

struct Ball {
    pos: Point2<Scalar>,
    ver: Vector2<Scalar>,
    hitbox: Hitbox,
}

impl Ball {
    pub fn update(&mut self, dt: Scalar) {
        self.pos += self.ver * dt;
        self.hitbox = self.gen_hitbox();
    }

    pub fn gen_hitbox(&self) -> Hitbox {
        gen_aabb_with_radius(self.center(), Ball::radius())
    }

    pub fn center(&self) -> Point2<Scalar> {
        self.pos
    }

    #[inline(always)]
    fn radius() -> Vector2<Scalar> {
        Vector2::new(0.01, 0.01)
    }
}

impl DrawSquareHitbox for Ball {
    fn hitbox(&self) -> Hitbox {
        self.hitbox
    }

    fn color(&self) -> Color {
        [1.0; 4]
    }
}

impl Default for Ball {
    fn default() -> Ball {
        Ball {
            pos: Point2::new(0.5, 0.5),
            ver: Vector2::new(0.0, 0.4),
            hitbox: Aabb2::new(Point2::new(0.01, 0.01), Point2::new(0.02, 0.02))
        }
    }
}

#[derive(Clone, Copy)]
enum WallCollision {
    Left,
    Top,
    Right,
    Bottom
}

struct Walls {
    hitboxes: [Hitbox; 4],
}

impl Walls {
    fn collision(&self, target: &Hitbox) -> Option<WallCollision> {
        use WallCollision::*;

        let table = [Left, Top, Right, Bottom];

        for (w, &k) in self.hitboxes.iter().zip(table.iter()) {
            if w.collide(target) {
                return Some(k);
            }
        }

        None
    }
}

impl Default for Walls {
    fn default() -> Walls {
        Walls {
            hitboxes: [Aabb2::new(Point2::new(-1.0, 0.0), Point2::new(0.0, 1.0)), // Left
                       Aabb2::new(Point2::new(0.0, -1.0), Point2::new(1.0, 0.0)), // Top
                       Aabb2::new(Point2::new(1.0, 0.0), Point2::new(2.0, 1.0)),  // Right
                       Aabb2::new(Point2::new(0.0, 1.0), Point2::new(1.0, 2.0))]  // Bottom
        }
    }
}

enum BlockCollision {
    Left,
    Up,
    Right,
    Down,
}

struct Block {
    hitbox: Hitbox,
    is_alive: bool,
    color: Color,
}

impl DrawSquareHitbox for Block {
    fn hitbox(&self) -> Hitbox {
        self.hitbox
    }

    fn color(&self) -> Color {
        if self.is_alive {
            self.color
        } else {
            [0.0; 4]
        }
    }
}

impl Block {
    fn new(hitbox: Hitbox, color: Color) -> Block {
        Block {
            hitbox: hitbox,
            is_alive: true,
            color: color,
        }
    }
}

struct Blocks {
    inner: Vec<Block>,
}

impl Blocks {
    fn draw(&self, args: &RenderArgs, gl: &mut GlGraphics) {
        for b in self.inner.iter() {
            b.draw(args, gl);
        }
    }

    fn with_grid(x: u32, y: u32) -> Blocks {
        let mut new = Blocks { inner: vec![] };

        let margin_x = 0.1;
        let margin_top = 0.1;
        let margin_bottom = 0.6;

        let width = (1.0 - margin_x * 2.) / (x as Scalar);
        let height = (1.0 - (margin_top + margin_bottom)) / (y as Scalar);

        for n in 0..x {
            for m in 0..y {
                new.inner.push(Block::new(Hitbox::new(Point2::new(margin_x + width * (n as Scalar), margin_top + height * (m as Scalar)),
                                                       Point2::new(margin_x + width * ((n + 1) as Scalar), margin_top + height * ((m + 1) as Scalar))),
                                          [1.0; 4]));
            }
        }

        new
    }

    fn collision(&mut self, target: &Hitbox) -> Option<BlockCollision> {
        use BlockCollision::*;

        for b in self.inner.iter_mut().filter(|b| b.is_alive) {
            if b.hitbox.collide(target) {
                b.is_alive = false;

                if (b.hitbox.min.y <= target.min.y && target.min.y <= b.hitbox.max.y) ||
                    (b.hitbox.min.y <= target.max.y && target.max.y <= b.hitbox.max.y) {

                    if b.hitbox.min.x <= target.max.x { return Some(Left); }
                    if b.hitbox.max.x >= target.min.x { return Some(Right); }
                    }
                if (b.hitbox.min.x <= target.min.x && target.min.x <= b.hitbox.max.x) ||
                    (b.hitbox.min.x <= target.max.x && target.max.x <= b.hitbox.max.x) {
                if b.hitbox.min.y <= target.max.y { return Some(Up); }
                if b.hitbox.max.y >= target.min.y { return Some(Down); }
                }
            }
        }

        None
    }
}

struct BreakOut {
    bar: Bar,
    ball: Ball,
    blocks: Blocks,
    walls: Walls,
}

impl BreakOut {
    fn new() -> BreakOut {
        BreakOut {
            bar: Default::default(),
            ball: Default::default(),
            blocks: Blocks::with_grid(5, 5),
            walls: Default::default(),
        }
    }

    fn draw(&self, args: &RenderArgs, gl: &mut GlGraphics) {
        gl.draw(args.viewport(), |_c, g| {
            graphics::clear([0.0; 4], g);
            self.bar.draw(args, g);
            self.ball.draw(args, g);
            self.blocks.draw(args, g);
        })
    }

    fn update(&mut self, dt: Scalar, cursor_pos: [f64; 2]) {
        self.bar.update(Point1::new(cursor_pos[0] as Scalar / WINDOW_WIDTH as Scalar));
        self.ball.update(dt);
        self.collision();
    }

    fn collision(&mut self) {
        if self.bar.hitbox().collide(&self.ball.hitbox()) {
            let v = Vector2::new(self.ball.center().x - self.bar.center().x, self.ball.center().y - self.bar.center().y);
            Basis2::from_angle(self.ball.ver.angle(v)).rotate_vector(self.ball.ver);
        }
        if let Some(w) = self.walls.collision(&self.ball.hitbox) {
            use WallCollision::*;

            match w {
                Left => self.ball.ver.x = self.ball.ver.x.abs(),
                Right => self.ball.ver.x = -self.ball.ver.x.abs(),
                Top => self.ball.ver.y = self.ball.ver.y.abs(),
                Bottom => self.ball.ver.y = -self.ball.ver.y.abs(),
            }
        }
        if let Some(b) = self.blocks.collision(&self.ball.hitbox()) {
            use BlockCollision::*;

            match b {
                Left => self.ball.ver.x = -self.ball.ver.x.abs(),
                Right => self.ball.ver.x = self.ball.ver.x.abs(),
                Up => self.ball.ver.y = -self.ball.ver.y.abs(),
                Down => self.ball.ver.y = self.ball.ver.y.abs(),
            }
        }
    }
}

fn main() {
    let mut window: PistonWindow =
            WindowSettings::new("BreakOut", [WINDOW_WIDTH, WINDOW_HEIGHT])
                    .exit_on_esc(true).build().unwrap();

    let v_opengl = OpenGL::V3_2;
    let mut gl = GlGraphics::new(v_opengl);

    let mut game = BreakOut::new();

    let mut cursor_pos = [(WINDOW_WIDTH / 2) as Scalar, 0.];
    while let Some(e) = window.next() {
        if let Some(r) = e.render_args() {
            game.draw(&r, &mut gl);
        }

        if let Some(u) = e.update_args() {
            game.update(u.dt as Scalar, cursor_pos);
        }

        if let Some(p) = e.mouse_cursor_args() {
            cursor_pos = p;        
        }
    }

}

